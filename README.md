Scope of this project is to create an event management system with following features:

Possibility for users to sign in, log in and register to events.
Possibility for admins to:
- add new events/locations.
- assign events to locations.
- view list of users for specified events
- view all events
- view locations
- delete users/events/locations 
- etc.

Technologies to be used for this project:
- ZIO framework 
- InMemoryDB to be changed to cassandra
- Add Swagger documentation using Tapir
- API description to be changed from ZIO to Tapir
- Apache Kafka(This application should be a producer of events in case some of the entities where added or updated)

Steps to run the app locally:

1.Pull and run docker container for cassandra:

        - docker run --rm --name cassandra -d cassandra:3.11 -p 9042:9042
        
2.Execute schema migrations either connecting to cassandra using:

        -  docker exec -it cassandra bash
        -  cqlsh
        -  execute queries to create keyspace and tables 
either install cassandra-migrate tool

package controller

import model.User
import service.UserService
import zio.ZIO
import zio.http._
import zio.json._

@Deprecated
object UserController {
  def apply(): Http[
    UserService,
    Serializable,
    Request,
    Response
  ] = {

    Http.collectZIO[Request] {
      case req @ (Method.POST -> Root / "user" / "add") =>
        for {
            body <- req.body.asString.map(user => user.fromJson[User])
            decodeResult <- body match {
              case Left(error) =>
                ZIO
                  .debug("failed")
                  .as(Response.text(error).withStatus(Status.BadRequest))
              case Right(deserializedUser: User) =>
                UserService
                  .signIn(
                    deserializedUser.name,
                    deserializedUser.age,
                    deserializedUser.email
                  )
                  .map(response => Response.json(response.toJson))
            }
          } yield decodeResult

      case req @ Method.GET -> Root / "user" if req.url.queryParams.nonEmpty =>
        UserService
          .getUserByEmail(req.url.queryParams.get("email").get.asString)
          .map(user => Response.json(user.toJson))

      case Method.GET -> Root / "users" =>
        UserService.getUsers
          .map(users => Response.json(users.toJson))
          .mapError(_ => new Throwable())

      case Method.PUT -> Root / "event" / eventId / "user" / userId =>
        UserService.assignUserToEvent(userId, eventId).map(rs => Response.json(rs.toJson))
    }
  }
}

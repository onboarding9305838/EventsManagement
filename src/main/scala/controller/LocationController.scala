package controller

import model.Location
import service.LocationService
import zio.ZIO
import zio.http.Status.BadRequest
import zio.http._
import zio.json.{DecoderOps, EncoderOps}

@Deprecated
object LocationController {

  def apply(): Http[LocationService, Serializable, Request, Response] = {
    Http.collectZIO[Request] {
      case req @ (Method.POST -> Root / "location" / "add") =>
        (
          for {
            body <- req.body.asString.map(_.fromJson[Location])
            decodedResult <- body match {
              case Left(error) =>
                ZIO
                  .debug("deserialization failed")
                  .as(Response.text(error.toString).withStatus(BadRequest))
              case Right(deserializedLocation) =>
                LocationService
                  .addLocation(deserializedLocation)
                  .map(Response.json(_))
            }
          } yield decodedResult
        )
      case Method.GET -> Root / "location" / id =>
        LocationService
          .getLocation(id)
          .map(location => Response.json(location.toJson))
    }
  }
}

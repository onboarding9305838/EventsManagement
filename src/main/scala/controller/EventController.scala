package controller

import model.Event
import service.EventService
import zio.ZIO
import zio.http._
import zio.json.{DecoderOps, EncoderOps}

@Deprecated
object EventController {

  def apply(): Http[EventService, Serializable, Request, Response] = {

    Http.collectZIO[Request] {
      case req @ (Method.POST -> Root / "event" / "add") =>
        for {
            body <- req.body.asString.map(event => event.fromJson[Event])
            result <- body match {
              case Left(error) =>
                ZIO
                  .debug("failed")
                  .as(Response.text(error).withStatus(Status.BadRequest))
              case Right(deserializedEvent) =>
                EventService
                  .addEvent(deserializedEvent)
                  .map(id => Response.json(id))
            }
          } yield result

      case Method.GET -> Root / "event" / id =>
        EventService.getEvent(id).map(event => Response.json(event.toJson))

      case Method.GET -> Root / "events" =>
        EventService.getEvents.map(events => Response.json(events.toJson))

      case Method.PUT -> Root / "location" / locationId / "event" / eventId =>
        EventService
          .assignEventToLocation(locationId, eventId)
          .map(res => Response.json(res.toJson))
    }
  }
}

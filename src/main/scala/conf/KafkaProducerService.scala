package conf

import zio.{&, Scope, Task, ZIO}

trait KafkaProducerService {

  def produce[T](topic: String, key: String, value: T): ZIO[Scope, Throwable, Unit]

}

object KafkaProducerService {
  def produce[T](topic: String, key: String, value:T): ZIO[KafkaProducerService&Scope, Throwable, Unit] = {
    ZIO.serviceWithZIO[KafkaProducerService](_.produce(topic, key, value))
  }
}
package conf

import model.Event
import zio.kafka.producer.{Producer, ProducerSettings}
import zio.kafka.serde.Serde
import zio.{Scope, Task, ZIO, ZLayer}

case class KafkaProducerServiceImpl() extends KafkaProducerService {

  override def produce[Event](
      topic: String,
      key: String,
      value: Event
  ): ZIO[Scope, Throwable, Unit] = {
    for {
      settings <- ZIO.succeed(ProducerSettings(List("localhost:9092")))
      producer <- Producer.make(settings)
      _ <- producer.produce(
        topic = topic,
        keySerializer = Serde.string,
        value = value.toString,
        valueSerializer = Serde.string,
        key = key
      )
    } yield (producer)
  }

}

object KafkaProducerServiceImpl {

  def layer: ZLayer[Nothing, Throwable, KafkaProducerServiceImpl] =
    ZLayer {
      for {
        _ <- ZIO.log("Registered Kafka layer")
      } yield KafkaProducerServiceImpl()
    }
}

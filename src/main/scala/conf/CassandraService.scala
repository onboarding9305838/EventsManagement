package conf

import com.datastax.oss.driver.api.core.cql.ResultSet
import zio.ZIO

trait CassandraService {
  def execute(query: String): ZIO[Any, Throwable, ResultSet]
}

object CassandraService {
  def execute(query: String): ZIO[CassandraService, Throwable, ResultSet] =
    ZIO.serviceWithZIO[CassandraService](_.execute(query))
}

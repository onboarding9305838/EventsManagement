package conf

import com.datastax.oss.driver.api.core.CqlSession
import com.datastax.oss.driver.api.core.cql.ResultSet
import zio.{ZIO, ZLayer}

case class CassandraServiceImpl(session: CqlSession) extends CassandraService {
  override def execute(query: String): ZIO[Any, Throwable, ResultSet] =
    ZIO.attempt(session.execute(query))
}

object CassandraServiceImpl {

  // Layer to provide CqlSession
  val sessionLayer: ZLayer[Any, Throwable, CqlSession] =
    ZLayer {
      //Can be configured with other custom params
      ZIO.attempt(
        CqlSession
          .builder()
          .withKeyspace("app")
          .build()
      )
    }

  // Layer to provide CassandraService using CqlSession
  val layer: ZLayer[CqlSession, Throwable, CassandraService] =
    ZLayer {
      for {
        session <- ZIO.service[CqlSession]
      } yield CassandraServiceImpl(session)
    }
}

package endpoints

import io.circe.generic.auto._
import model.Event
import service.EventService
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.{PublicEndpoint, endpoint, path, stringBody}
import zio.ZIO
import sttp.tapir._

object EventEndpoints {

  val addEventEndpoint: PublicEndpoint[Event, String, String, Any] = {
    endpoint.post
      .in("event")
      .in(jsonBody[Event])
      .errorOut(stringBody)
      .out(jsonBody[String])
  }

  val getEventByIdEndpoint: PublicEndpoint[String, String, Event, Any] = {
    endpoint
      .in("event"/path[String]("id"))
      .errorOut(stringBody)
      .out(jsonBody[Event])
  }

  val getEventsEndpoint: PublicEndpoint[Unit, String, List[Event], Any] = {
    endpoint.get
      .in("events")
      .errorOut(stringBody)
      .out(jsonBody[List[Event]])

  }

  val assignEventToLocationEndpoint: PublicEndpoint[(String, String), String, Boolean, Any] ={
    endpoint.put
      .in("event"/path[String]("eventId")/"location"/path[String]("locationId"))
      .errorOut(stringBody)
      .out(jsonBody[Boolean])
  }

  def addEvent: Event => ZIO[EventService, String, String] = {
    event => EventService.addEvent(event)
  }

  def getEventById: String => ZIO[EventService, String, Event] = {
    eventId => EventService.getEvent(eventId)
  }

  def getEvents: Unit => ZIO[EventService, String, List[Event]] = {
    (_) => EventService.getEvents
  }

  def assignEventToLocation: (String, String) => ZIO[EventService, String, Boolean] ={
    (eventId, locationId) => EventService.assignEventToLocation(locationId, eventId)
  }

}

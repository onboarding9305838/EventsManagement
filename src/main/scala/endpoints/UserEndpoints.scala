package endpoints

import io.circe.generic.auto._
import model.User
import service.UserService
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe.jsonBody
import sttp.tapir._
import zio.ZIO
object UserEndpoints {
  val usersEndpoint: PublicEndpoint[Unit, String, List[User], Any] = {
    endpoint.get
      .in("users")
      .errorOut(stringBody)
      .out(jsonBody[List[User]])
  }

  val userGetByIdEndpoint: PublicEndpoint[String, String, User, Any] = {
    endpoint.get
      .in("user" / path[String]("id"))
      .errorOut(stringBody)
      .out(jsonBody[User])
  }

  val userGetByEmailEndpoint: PublicEndpoint[String, String, User, Any] = {
    endpoint.get
      .in("user" / query[String]("email"))
      .errorOut(stringBody)
      .out(jsonBody[User])
  }

  val addUserEndpoint: PublicEndpoint[User, String, String, Any] = {
    endpoint.post
      .in("user")
      .in(jsonBody[User])
      .errorOut(stringBody)
      .out(jsonBody[String])
  }

  val assignUserToEventEndpoint: PublicEndpoint[(String, String), String, Boolean, Any] = {
    endpoint.put
      .in("user" / path[String]("id") / "event" / path[String]("eventId"))
      .errorOut(stringBody)
      .out(jsonBody[Boolean])
  }

  def getUsers: Unit => ZIO[UserService, String, List[User]] = { _ =>
    UserService.getUsers
  }

  def getUserById: String => ZIO[UserService, String, User] = { id =>
    UserService.getUserById(id)
  }

  def addUser: User => ZIO[UserService, String, String] = { user =>
    UserService.signIn(user.name, user.age, user.email)
  }

  def getUserByEmail: String => ZIO[UserService, String, User] = { email =>
    UserService.getUserByEmail(email)
  }

  def assignUserToEvent: (String, String) => ZIO[UserService, String, Boolean] = {
    (userId, eventId) => UserService.assignUserToEvent(userId, eventId)
  }
}

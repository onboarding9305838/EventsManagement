package endpoints

import model.Location
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.{PublicEndpoint, endpoint, path, stringBody}
import io.circe.generic.auto._
import service.LocationService
import sttp.tapir.generic.auto.schemaForCaseClass
import zio.ZIO
import sttp.tapir._

object LocationEndpoints {

  val addLocationEndpoint: PublicEndpoint[Location, String, String, Any] ={
    endpoint.post
      .in("location")
      .in(jsonBody[Location])
      .errorOut(stringBody)
      .out(jsonBody[String])
  }

  val getLocationEndpoint: PublicEndpoint[String, String, Location, Any] = {
    endpoint.get
      .in("location"/path[String]("id"))
      .errorOut(stringBody)
      .out(jsonBody[Location])
  }

  def addLocation: Location => ZIO[LocationService, String, String] ={
    location => LocationService.addLocation(location)
  }

  def getLocation: String => ZIO[LocationService, String, Location] = {
    locationId => LocationService.getLocation(locationId)
  }

}

package service

import com.datastax.oss.driver.api.core.cql.{ResultSet, Row}
import conf.CassandraService
import model.{Event, User}
import zio.{ZIO, ZLayer}

import java.util.UUID

case class UserServiceImpl(cassandraService: CassandraService) extends UserService {
  private def rowToUser(row: Row): User =
    User(row.getString("name"), row.getInt("age"), row.getString("email"))

  private def rowToEvent(row: Row): Event =
    Event(row.getString("eventName"), row.getString("description"))
  // Get user by email
  override def getUserByEmail(email: String): ZIO[Any, String, User] = {
    val selectQuery =
      s"SELECT * FROM app.users WHERE email='$email' ALLOW FILTERING;"
    cassandraService
      .execute(selectQuery)
      .map(rs =>
        Option(rs.one())
          .map(rowToUser)
          .getOrElse(throw new Exception("User not found"))
      ).mapError(_.getMessage)
  }

  def signIn(
      name: String,
      age: Int,
      email: String
  ): ZIO[Any, String, String] = {
    val id = UUID.randomUUID()
    val insertQuery =
      s"INSERT INTO app.users (id, name, age, email) VALUES ($id,'$name', $age,'$email');"
    cassandraService.execute(insertQuery).map(_ => id.toString).mapError(_.getMessage)
  }

  def getUsers: ZIO[Any, String, List[User]] = {
    val query = "SELECT * FROM users;"
    cassandraService
      .execute(query)
      .map(_.all().toArray.toList.map { case row: Row =>
        rowToUser(row)
      })
      .mapError(_.getStackTrace.mkString("Array(", ", ", ")"))
  }

  override def assignUserToEvent(
      userId: String,
      eventId: String
  ): ZIO[Any, String, Boolean] = {

    val findUserQuery  = s"SELECT * from app.users where id = $userId"
    val findEventQuery = s"SELECT * from app.events where id = $eventId"

    //TODO add batch operations
    for {
      user <- cassandraService
        .execute(findUserQuery)
        .map(rs =>
          Option(rs.one())
            .map(rowToUser)
            .getOrElse(throw new Exception("User not found"))
        )
      event <- cassandraService
        .execute(findEventQuery)
        .map(rs =>
          Option(rs.one())
            .map(rowToEvent)
            .getOrElse(throw new Exception("Event not found"))
        )
      insertIntoUsersToEventQuery: String = s"" +
        s"INSERT INTO app.UserByEvent(event_id, user_id, user_name, event_name) " +
        s"VALUES ($eventId,$userId,'${user.name}','${event.eventName}');"
      insertIntoEventToUsersQuery: String =
        s"INSERT INTO app.EventByUser(user_id, event_id, user_name, event_name) " +
          s"VALUES ($eventId,$userId,'${user.name}','${event.eventName}');"
      _ <- ZIO.logInfo(s"Assigning ${user.name} to event ${event.eventName}")
      _ <- cassandraService.execute(insertIntoEventToUsersQuery)
      _ <- cassandraService.execute(insertIntoUsersToEventQuery)
    } yield true
  }.mapError(_.getMessage)

  override def getUserById(id: String): ZIO[Any, String, User] = {
    val getUserByIdQuery: String = s"SELECT * FROM app.users where id = ${id}"
    cassandraService
      .execute(getUserByIdQuery)
      .map(rs =>
        Option(rs.one())
          .map(rowToUser)
          .getOrElse(throw new Exception("UserNotFound"))
      )
      .mapError(_.getStackTrace.mkString("Array(", ", ", ")"))
  }
}

object UserServiceImpl {

  def layer: ZLayer[CassandraService, Throwable, UserServiceImpl] =
    ZLayer {
      for {
        cassandra <- ZIO.service[CassandraService]
      } yield UserServiceImpl(cassandra)
    }

}

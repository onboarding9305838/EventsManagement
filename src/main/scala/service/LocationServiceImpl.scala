package service

import com.datastax.oss.driver.api.core.cql.Row
import conf.CassandraService
import model.Location
import zio.{Task, ZIO, ZLayer}

import java.util.UUID

case class LocationServiceImpl(cassandra: CassandraService) extends LocationService {
  private def mapRowToLocation(row: Row): Location = {
    Location(row.getString("locationName"), row.getInt("capacity"))
  }
  override def addLocation(location: Location): ZIO[Any, String,String] = {
    val id = UUID.randomUUID()
    val addLocationQuery =
      s"Insert into app.locations(id, locationName, capacity) VALUES($id, '${location.locationName}', ${location.capacity});"
    cassandra.execute(addLocationQuery).map(_ => id.toString)
  }.mapError(_.getMessage)

  override def getLocation(locationId: String): ZIO[Any, String, Location] = {
    val getLocation = s"SELECT * from app.locations where id=$locationId;"

    cassandra
      .execute(getLocation)
      .map(response =>
        Option(response.one())
          .map(mapRowToLocation)
          .getOrElse(throw new Exception("Location not found"))
      )
  }.mapError(_.getMessage)
}

object LocationServiceImpl {
  def layer: ZLayer[
    CassandraService,
    Nothing,
    LocationServiceImpl
  ] = {
    ZLayer {
      for {
        cassandra <- ZIO.service[CassandraService]
      } yield LocationServiceImpl(cassandra)
    }
  }
}

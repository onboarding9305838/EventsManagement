package service

import com.datastax.oss.driver.api.core.cql.Row
import conf.CassandraService
import model.{Event, Location}
import zio.{ZIO, ZLayer}

import java.util.UUID

case class EventServiceImpl(cassandra: CassandraService) extends EventService {

  private def mapRowToEvent(row: Row): Event = {
    Event(row.getString("eventName"), row.getString("description"))
  }

  private def mapRowToLocation(row: Row): Location = {
    Location(row.getString("locationName"), row.getInt("capacity"))
  }

  override def addEvent(event: Event): ZIO[Any,String, String] = {
    val id = UUID.randomUUID()
    val addEventQuery =
      s"Insert INTO app.events(id, eventName, description) VALUES($id,'${event.eventName}','${event.description}');"
    cassandra.execute(addEventQuery).map(_ => id.toString).mapError(_.getMessage)
  }

  override def getEvent(id: String): ZIO[Any, String, Event] = {
    val getEventQuery: String = s"SELECT * FROM app.events where id= $id"
    cassandra.execute(getEventQuery).map(_.one()).map(mapRowToEvent).mapError(_.getMessage)
  }

  override def getEvents: ZIO[Any, String, List[Event]] = {
    val selectAllEventsQuery: String = "SELECT * FROM app.events"
    cassandra
      .execute(selectAllEventsQuery)
      .map(_.all().toArray.toList.map { case row: Row =>
        mapRowToEvent(row)
      }).mapError(_.getMessage)
  }

  override def assignEventToLocation(
      locationId: String,
      eventId: String
  ): ZIO[Any, String, Boolean] = {
    val getLocationQuery = s"Select * from app.locations where id =$locationId;"
    val getEventQuery    = s"SELECT * from app.events where id =$eventId;"

    for {
      location <- cassandra
        .execute(getLocationQuery)
        .map(response =>
          Option(response.one())
            .map(mapRowToLocation)
            .getOrElse(throw new Exception("Location not found"))
        )
      event <- cassandra
        .execute(getEventQuery)
        .map(response =>
          Option(response.one())
            .map(mapRowToEvent)
            .getOrElse(throw new Exception("Event not found"))
        )

      _ <- ZIO.logInfo(s"Assigning event ${event.eventName} with location ${location.locationName}")
      assignEventToLocationQuery: String =
        "Insert into app.EventsByLocation(location_id,event_id," +
          s"location_name,event_name) VALUES($locationId, $eventId, '${location.locationName}', '${event.eventName}')"

      bool <- cassandra.execute(assignEventToLocationQuery).map(_ => true)

    } yield bool

  }.mapError(_.getMessage)
}

object EventServiceImpl {
  def layer: ZLayer[
    CassandraService,
    Nothing,
    EventServiceImpl
  ] = ZLayer {
    for {
      cassandra <- ZIO.service[CassandraService]
    } yield EventServiceImpl(cassandra)
  }
}

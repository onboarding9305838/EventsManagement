package service

import model.Location
import zio.{Task, ZIO}

trait LocationService {
  def addLocation(location: Location): ZIO[Any, String, String]

  def getLocation(locationId: String): ZIO[Any, String, Location]
}

object LocationService {
  def addLocation(location: Location): ZIO[LocationService, String, String] =
    ZIO.serviceWithZIO[LocationService].apply(_.addLocation(location))

  def getLocation(id: String): ZIO[LocationService, String, Location] = {
    ZIO.serviceWithZIO[LocationService](_.getLocation(id))
  }
}

package service

import model.Event
import zio.ZIO

trait EventService {

  def addEvent(event: Event): ZIO[Any, String, String]

  def getEvent(id: String): ZIO[Any, String, Event]

  def getEvents: ZIO[Any, String, List[Event]]

  def assignEventToLocation(
      locationId: String,
      eventId: String
  ): ZIO[Any, String, Boolean]
}

object EventService {
  def getEvent(id: String): ZIO[EventService, String, Event] =
    ZIO.serviceWithZIO[EventService](_.getEvent(id))

  def addEvent(event: Event): ZIO[EventService, String, String] =
    ZIO.serviceWithZIO[EventService](
      _.addEvent(event)
    )

  def getEvents: ZIO[EventService, String, List[Event]] =
    ZIO.serviceWithZIO[EventService](
      _.getEvents
    )

  def assignEventToLocation(
      locationId: String,
      eventId: String
  ): ZIO[EventService, String, Boolean] = {
    ZIO.serviceWithZIO[EventService](
      _.assignEventToLocation(locationId, eventId)
    )
  }
}

package service

import model.User
import zio.ZIO

trait UserService {
  def signIn(
      name: String,
      age: Int,
      email: String
  ): ZIO[Any, String, String]

  def getUsers: ZIO[Any, String, List[User]]

  def getUserById(id:String): ZIO[Any, String, User]

  def getUserByEmail(email: String): ZIO[Any, String, User]

  def assignUserToEvent(
      userId: String,
      eventId: String
  ): ZIO[Any, String, Boolean]
}

object UserService {
  def getUserById(id: String): ZIO[UserService, String, User] = {
    ZIO.serviceWithZIO[UserService](_.getUserById(id))
  }

  def signIn(
      name: String,
      age: Int,
      email: String
  ): ZIO[UserService, String, String] =
    ZIO.serviceWithZIO[UserService](_.signIn(name, age, email))

  def getUsers: ZIO[UserService, String, List[User]] =
    ZIO.serviceWithZIO[UserService](_.getUsers)

  def getUserByEmail(
      email: String
  ): ZIO[UserService, String, User] =
    ZIO.serviceWithZIO[UserService](_.getUserByEmail(email))

  def assignUserToEvent(
      userId: String,
      eventId: String
  ): ZIO[UserService, String, Boolean] =
    ZIO.serviceWithZIO[UserService](
      _.assignUserToEvent(userId, eventId)
    )
}

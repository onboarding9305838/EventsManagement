package model

import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class Location(
    locationName: String,
    capacity: Int
)

object Location {
  implicit val fromJson: JsonDecoder[Location] = DeriveJsonDecoder.gen[Location]

  implicit val toJson: JsonEncoder[Location] = DeriveJsonEncoder.gen[Location]

}

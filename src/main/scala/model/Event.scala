package model

import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class Event(
    eventName: String,
    description: String,
)

object Event {
  //TODO Add Circe
  implicit val fromJson: JsonDecoder[Event] = DeriveJsonDecoder.gen[Event]

  implicit val toJson: JsonEncoder[Event] = DeriveJsonEncoder.gen[Event]
}

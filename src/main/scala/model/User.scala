package model

import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class User(
    name: String,
    age: Int,
    email: String
)

object User {
  implicit val fromJson: JsonDecoder[User] =
    DeriveJsonDecoder.gen[User]

  implicit val toJson: JsonEncoder[User] =
    DeriveJsonEncoder.gen[User]
}

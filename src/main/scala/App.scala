import conf.{CassandraServiceImpl, KafkaProducerServiceImpl}
import controller.LocationController
import endpoints.EventEndpoints.{addEvent, addEventEndpoint, assignEventToLocation, assignEventToLocationEndpoint, getEventById, getEventByIdEndpoint, getEvents, getEventsEndpoint}
import endpoints.LocationEndpoints.{addLocation, addLocationEndpoint, getLocation, getLocationEndpoint}
import endpoints.UserEndpoints._
import service._
import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.ztapir.RichZEndpoint
import zio.http.Server
import zio.{Scope, ZIO, ZIOAppArgs, ZIOAppDefault}
object App extends ZIOAppDefault {
  override def run: ZIO[Any with ZIOAppArgs with Scope, Any, Any] = {

    val getUserRoutes =
      ZioHttpInterpreter().toHttp(
        usersEndpoint.zServerLogic[UserService](
          getUsers
        )
      )

    val getUserByIdRoute = ZioHttpInterpreter().toHttp(
      userGetByIdEndpoint.zServerLogic[UserService](getUserById)
    )

    val addUserRoute = ZioHttpInterpreter().toHttp(
      addUserEndpoint.zServerLogic[UserService](addUser)
    )

    val getUserByEmailRoute = ZioHttpInterpreter().toHttp(
      userGetByEmailEndpoint.zServerLogic[UserService](getUserByEmail)
    )

    val assignUserToEventRoute = ZioHttpInterpreter().toHttp(
      assignUserToEventEndpoint.zServerLogic[UserService](userEventData =>
        assignUserToEvent(userEventData._1, userEventData._2)
      )
    )

    val addEventRoute = ZioHttpInterpreter()
      .toHttp(addEventEndpoint.zServerLogic[EventService](addEvent))

    val getEventRoute = ZioHttpInterpreter()
      .toHttp(getEventByIdEndpoint.zServerLogic[EventService](getEventById))

    val getEventsRoute = ZioHttpInterpreter()
      .toHttp(getEventsEndpoint.zServerLogic[EventService](getEvents))

    val assignEventToLocationRoute = ZioHttpInterpreter()
      .toHttp(assignEventToLocationEndpoint.zServerLogic[EventService](
        eventToLocationData =>
        assignEventToLocation(eventToLocationData._1 ,eventToLocationData._2)))

    val addLocationRoute = ZioHttpInterpreter().toHttp(addLocationEndpoint.zServerLogic[LocationService](addLocation))

    val getLocationRoute = ZioHttpInterpreter().toHttp(getLocationEndpoint.zServerLogic[LocationService](getLocation))

    val https = getUserRoutes ++ getUserByIdRoute ++
        addUserRoute ++ getUserByEmailRoute ++ assignUserToEventRoute ++ addEventRoute ++
        getEventRoute ++ getEventsRoute ++assignEventToLocationRoute ++ addLocationRoute ++
        getLocationRoute

    Server
      .serve(https.withDefaultErrorResponse)
      .provide(
        Server.defaultWithPort(8080),
        CassandraServiceImpl.sessionLayer,
        CassandraServiceImpl.layer,
        UserServiceImpl.layer,
        EventServiceImpl.layer,
        LocationServiceImpl.layer,
        KafkaProducerServiceImpl.layer
      )
  }
}

package repository

import model.Event
import zio.{Task, UIO, ZIO}

trait EventInMemoryRepository {
  def save(event: Event): Task[String]

  def getEvent(id: String): Task[Option[Event]]

  def getEvents: UIO[List[Event]]
}

object EventInMemoryRepository {
  def save(event: Event): ZIO[EventInMemoryRepository, Throwable, String] =
    ZIO.serviceWithZIO[EventInMemoryRepository](_.save(event))

  def getEvent(id: String): ZIO[EventInMemoryRepository, Throwable, Option[Event]] =
    ZIO.serviceWithZIO[EventInMemoryRepository](_.getEvent(id))

  def getEvents: ZIO[EventInMemoryRepository, Throwable, List[Event]] = {
    ZIO.serviceWithZIO[EventInMemoryRepository](_.getEvents)
  }
}

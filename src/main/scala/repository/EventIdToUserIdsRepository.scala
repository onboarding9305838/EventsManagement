package repository

import zio.ZIO

trait EventIdToUserIdsRepository {

  def assignUserToEvent(
      userId: String,
      eventId: String
  ): ZIO[Any, Throwable, Unit]

}

object EventIdToUserIdsRepository {

  def assignUserToEvent(
      userId: String,
      eventId: String
  ): ZIO[EventIdToUserIdsRepository, Throwable, Unit] =
    ZIO.serviceWithZIO[EventIdToUserIdsRepository](
      _.assignUserToEvent(userId, eventId)
    )

}

package repository

import model.Location
import zio.{Task, ZIO}

trait LocationInMemoryRepository {
  def save(location: Location): Task[String]

  def find(locationId: String): Task[Option[Location]]
}

object LocationInMemoryRepository {
  def save(
      location: Location
  ): ZIO[LocationInMemoryRepository, Throwable, String] = {
    ZIO.serviceWithZIO.apply(_.save(location))
  }

  def find(
      locationId: String
  ): ZIO[LocationInMemoryRepository, Throwable, Option[Location]] = {
    ZIO.serviceWithZIO(_.find(locationId))
  }
}

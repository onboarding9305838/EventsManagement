package repository
import model.User
import zio.{Random, Ref, Task, ZLayer}

@Deprecated
case class UserInMemoryRepositoryImpl(userMap: Ref[Map[String, User]])
    extends UserInMemoryRepository {
  override def save(user: User): Task[String] = for {
    id <- Random.nextUUID.map(_.toString)
    _  <- userMap.update(_ + (id -> user))
  } yield id
}

@Deprecated
object UserInMemoryRepositoryImpl {
  def layer: ZLayer[Any, Nothing, UserInMemoryRepositoryImpl] = {
    ZLayer.fromZIO(
      Ref.make(Map.empty[String, User]).map(new UserInMemoryRepositoryImpl(_))
    )
  }
}

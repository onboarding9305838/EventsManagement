package repository

import zio.ZIO

trait LocationIdToEventIdsRepository {
  def getEventsIdForLocation(
      locationId: String
  ): ZIO[Any, Throwable, List[String]]

  def assignEventToLocation(
      eventId: String,
      locationId: String
  ): ZIO[Any, Throwable, Unit]

}


object LocationIdToEventIdsRepository {

  def assignEventToLocation(
      eventId: String,
      locationId: String
  ): ZIO[LocationIdToEventIdsRepository, Throwable, Unit] = {
    ZIO.serviceWithZIO[LocationIdToEventIdsRepository](
      _.assignEventToLocation(eventId, locationId)
    )
  }

  def getEventsIdForLocation(
      locationId: String
  ): ZIO[LocationIdToEventIdsRepository, Throwable, List[String]] = {
    ZIO.serviceWithZIO[LocationIdToEventIdsRepository](
      _.getEventsIdForLocation(locationId)
    )
  }
}

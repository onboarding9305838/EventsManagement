package repository
import model.Event
import zio.{Random, Ref, Task, UIO, ZLayer}

@Deprecated
case class EventInMemoryImpl(eventMap: Ref[Map[String, Event]])
    extends EventInMemoryRepository {
  override def save(event: Event): Task[String] = for {
    id <- Random.nextUUID.map(_.toString)
    _  <- eventMap.update(_ + (id -> event))
  } yield id

  override def getEvent(id: String): Task[Option[Event]] = eventMap.get.map(
    _.get(id)
  )

  override def getEvents: UIO[List[Event]] = eventMap.get.map(_.values.toList)
}

@Deprecated
object EventInMemoryImpl {
  def layer: ZLayer[Any, Nothing, EventInMemoryImpl] = {
    ZLayer.fromZIO(Ref.make(Map.empty[String, Event]).map(EventInMemoryImpl(_)))
  }
}

package repository

import model.User
import zio.{Task, ZIO}

trait UserInMemoryRepository {
  def save(user: User): Task[String]
}

object UserInMemoryRepository {
  def save(user: User): ZIO[UserInMemoryRepository, Throwable, String] = {
    ZIO.serviceWithZIO[UserInMemoryRepository](_.save(user))
  }
}

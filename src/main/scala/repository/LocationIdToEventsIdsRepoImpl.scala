package repository
import model.Event
import zio.{Ref, ZIO, ZLayer}

@Deprecated
case class LocationIdToEventsIdsRepoImpl(
    locationIdToEventsId: Ref[Map[String, List[String]]]
) extends LocationIdToEventIdsRepository {
  override def assignEventToLocation(
      eventId: String,
      locationId: String
  ): ZIO[Any, Throwable, Unit] = {
    val updateLocationIdToEvents: ZIO[Any, Throwable, Unit] = for {
      currentMap <- locationIdToEventsId.get
      eventIds        = currentMap.getOrElse(locationId, List.empty[String])
      updatedEventIds = eventId :: eventIds
      updatedMap      = currentMap.updated(locationId, updatedEventIds)
      _ <- locationIdToEventsId.set(updatedMap)
    } yield ()
    updateLocationIdToEvents
  }

  override def getEventsIdForLocation(
      locationId: String
  ): ZIO[Any, Throwable, List[String]] = {
    val value =
      locationIdToEventsId.get.map(_.getOrElse(locationId, List.empty))
    ZIO.log(value.toString)
    value
  }
}

@Deprecated
object LocationIdToEventsIdsRepoImpl {

  def layer: ZLayer[Any, Nothing, LocationIdToEventIdsRepository] = {
    ZLayer.fromZIO(
      Ref
        .make(Map.empty[String, List[String]])
        .map(LocationIdToEventsIdsRepoImpl(_))
    )
  }

}

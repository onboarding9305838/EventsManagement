package repository

import model.Location
import zio.{Random, Ref, Task, ZLayer}

@Deprecated
case class LocationInMemoryImpl(locationMap: Ref[Map[String, Location]])
    extends LocationInMemoryRepository {
  override def save(location: Location): Task[String] = for {
    id <- Random.nextUUID.map(_.toString)
    _  <- locationMap.update(_ + (id -> location))
  } yield id

  override def find(locationId: String): Task[Option[Location]] =
    locationMap.get.map(_.get(locationId))
}
@Deprecated
object LocationInMemoryImpl {
  def layer: ZLayer[Any, Nothing, LocationInMemoryImpl] = {
    ZLayer.fromZIO(
      Ref.make(Map.empty[String, Location]).map(new LocationInMemoryImpl(_))
    )
  }
}

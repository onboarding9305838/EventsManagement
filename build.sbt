ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"
val zioVersion = "2.0.15"
resolvers += "Sonatype OSS Snapshots" at "https://s01.oss.sonatype.org/content/repositories/snapshots"
resolvers += "Maven Central" at "https://repo1.maven.org/maven2/"
libraryDependencies += "dev.zio" %% "zio" % "2.0.13"
libraryDependencies += "dev.zio" %% "zio-streams" % "2.0.6"
libraryDependencies ++= Seq(
  "dev.zio"       %% "zio-json"       % "0.6.0",
  "dev.zio"       %% "zio-http"       % "3.0.0-RC2",
)
val circeVersion = "0.14.3"
val jackSonVersion = "2.15.2"
libraryDependencies += "com.datastax.oss" % "java-driver-core" % "4.17.0"

// https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "2.0.5"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-zio-http" % "0.18.3"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-zio-http-server" % "1.7.5"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-zio" % "1.7.5"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-core" % "1.2.10"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % "1.2.10"
libraryDependencies += "dev.zio" %% "zio-kafka" % "2.3.0"

lazy val root = (project in file("."))
  .settings(
    name := "EvManagement"
  )
